﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            double distance = 30;
            double deltaLat = 360 * distance / EarthLength;
            double deltaLong = deltaLat / Math.Cos(DegreesToRadians(53.895156));
            Console.WriteLine($"{53.895156 - deltaLat}, {27.376682-deltaLong}");
            Console.ReadLine();
            

        }
        private const double EarthLength = 40075;

        private static double DegreesToRadians(double degrees)
        {
            return degrees * Math.PI / 180;
        }
    }
}
